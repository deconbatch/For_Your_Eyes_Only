/**
 * For Your Eyes Only.
 * curve line to straight line animation.
 * 
 * Processing 3.2.1
 * @author @deconbatch
 * @version 0.1
 * created 0.1 2019.12.10
 */

void setup() {

  size(720, 720);
  colorMode(HSB, 360.0, 100.0, 100.0, 100.0);
  smooth();
  noLoop();

}

void draw() {

  int   frmMax     = 24 * 5; // for 24fps x 5sex animation
  int   nodeMax    = 60;
  float initRadius = width * 0.02;
  float waveFactor = random(0.05, 0.15);
  float baseHue    = random(360.0);
  ArrayList<PVector> circlePoints = new ArrayList<PVector>();
  
  translate(width * 0.5, height * 0.5);
  rotate(random(TWO_PI));

  for (int frmCnt = 0; frmCnt < frmMax; frmCnt++) {

    float frmRatio   = map(frmCnt, 0, frmMax, 0.0, 1.0);
    float easeRatio  = frmRatio * frmRatio;
    float baseRadius = width * map(easeRatio, 0.0, 1.0, 0.8, 0.5);
    int   circleMax  = floor(constrain(map(easeRatio, 0.0, 1.0, 120, 5), 5, 60));

    background((baseHue + 30.0) % 360.0, 100.0, 40.0, 100.0);
    circlePoints.clear();

    for (int circleCnt = 0; circleCnt < circleMax; circleCnt++) {
   
      float circleRatio = map(circleCnt, 0, circleMax, 0.0, 1.0);
      float flatRatio   = (frmRatio + circleRatio) % 1.0;
   
      float shapeX = initRadius * cos(TWO_PI / circleMax * circleCnt);
      float shapeY = initRadius * sin(TWO_PI / circleMax * circleCnt);

      //      float sHue = (baseHue + circleRatio * 90.0 * (1.0 - frmRatio)) % 360.0;
      float sHue = (baseHue + circleRatio * 60.0) % 360.0;
      float sSat = 80.0 - 20.0 * frmRatio;
      float sBri = 100.0 - map(sin(PI * circleRatio), 0.0, 1.0, 70.0, 0.0) * (1.0 - frmRatio);
      stroke(sHue, sSat, sBri, 100.0);
      strokeWeight((3.0 + circleRatio * map(frmRatio, 0.0, 1.0, 15.0, 5.0)));
 
      for (int nodeCnt = 0; nodeCnt < nodeMax; nodeCnt++) {
        float nodeRatio  = map(nodeCnt, 0, nodeMax, 0.0, 1.0);
        float nodeRadian = TWO_PI * (nodeRatio + sin(TWO_PI * flatRatio) * waveFactor);
        float bendRadius = map(sin(TWO_PI * (nodeRatio + flatRatio)), -1.0, 1.0, 0.95, 1.05);
        float nodeRadius = baseRadius * (circleRatio + 0.1) * bendRadius;
        float nX = nodeRadius * cos(nodeRadian) + shapeX;
        float nY = nodeRadius * sin(nodeRadian) + shapeY;
        if (circlePoints.size() == nodeMax) {
          line(circlePoints.get(nodeCnt).x, circlePoints.get(nodeCnt).y, nX, nY);
          circlePoints.set(nodeCnt, new PVector(nX, nY));
        } else {
          circlePoints.add(new PVector(nX, nY));
        }
      }
    }
    saveFrame("frames/00." + String.format("%04d", frmCnt) + ".png");
  }

  // for 2sec still image
  for (int i = 0; i < 24 * 2; i++) {
    saveFrame("frames/01." + String.format("%04d", i) + ".png");
  }
  exit();
}
